﻿namespace LiveHikVision
{
    partial class frmLiveView
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLiveView));
            this.treeView = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tmrGraba = new System.Windows.Forms.Timer(this.components);
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAudioIP = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.btnGrabaVideo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCaptura = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnLogOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lblEstado = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAgregaRCP = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.lblEstadoCliente = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.tmrEnLinea = new System.Windows.Forms.Timer(this.components);
            this.tmrToolStrip = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.lblEquipo = new System.Windows.Forms.Label();
            this.picLogoDisp = new System.Windows.Forms.PictureBox();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.canalZeroView = new System.Windows.Forms.PictureBox();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.liveView = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.splitContainer7 = new System.Windows.Forms.SplitContainer();
            this.playbackView = new System.Windows.Forms.PictureBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.dtpHoraIni = new System.Windows.Forms.DateTimePicker();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.btnStop = new System.Windows.Forms.ToolStripButton();
            this.btnPlay = new System.Windows.Forms.ToolStripButton();
            this.btnPause = new System.Windows.Forms.ToolStripButton();
            this.btnSlow = new System.Windows.Forms.ToolStripButton();
            this.btnFast = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCapturaPB = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.lblEnlace = new System.Windows.Forms.ToolStripLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.dttpHoraFin = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.rbtnMain = new System.Windows.Forms.RadioButton();
            this.rbtnSub = new System.Windows.Forms.RadioButton();
            this.btnAudio = new System.Windows.Forms.ToolStripDropDownButton();
            this.tstAudio1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tstAudio2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogoDisp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canalZeroView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.liveView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).BeginInit();
            this.splitContainer7.Panel1.SuspendLayout();
            this.splitContainer7.Panel2.SuspendLayout();
            this.splitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playbackView)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(105)))), ((int)(((byte)(178)))));
            this.treeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.treeView.ForeColor = System.Drawing.SystemColors.Info;
            this.treeView.FullRowSelect = true;
            this.treeView.ImageIndex = 1;
            this.treeView.ImageList = this.imageList1;
            this.treeView.ItemHeight = 25;
            this.treeView.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(205)))), ((int)(((byte)(219)))));
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Margin = new System.Windows.Forms.Padding(0);
            this.treeView.Name = "treeView";
            this.treeView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.treeView.SelectedImageIndex = 2;
            this.treeView.ShowLines = false;
            this.treeView.ShowPlusMinus = false;
            this.treeView.Size = new System.Drawing.Size(254, 713);
            this.treeView.TabIndex = 0;
            this.treeView.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterCheck);
            this.treeView.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView_NodeMouseClick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "dvr.png");
            this.imageList1.Images.SetKeyName(1, "agila.png");
            this.imageList1.Images.SetKeyName(2, "Aguila-play.png");
            this.imageList1.Images.SetKeyName(3, "cam21.png");
            this.imageList1.Images.SetKeyName(4, "cam23.png");
            this.imageList1.Images.SetKeyName(5, "cam.jpg");
            this.imageList1.Images.SetKeyName(6, "cam1.png");
            this.imageList1.Images.SetKeyName(7, "cam11.ico");
            // 
            // tmrGraba
            // 
            this.tmrGraba.Interval = 30000;
            this.tmrGraba.Tick += new System.EventHandler(this.tmrGraba_Tick);
            // 
            // toolStrip
            // 
            this.toolStrip.AutoSize = false;
            this.toolStrip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAudio,
            this.toolStripSeparator1,
            this.btnAudioIP,
            this.toolStripSeparator6,
            this.btnGrabaVideo,
            this.toolStripSeparator2,
            this.btnCaptura,
            this.toolStripSeparator3,
            this.btnLogOut,
            this.toolStripSeparator4,
            this.lblEstado,
            this.toolStripSeparator5,
            this.btnAgregaRCP,
            this.toolStripSeparator10,
            this.lblEstadoCliente,
            this.toolStripSeparator11});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStrip.Size = new System.Drawing.Size(808, 42);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "toolStrip1";
            this.toolStrip.MouseLeave += new System.EventHandler(this.toolStrip_MouseLeave);
            this.toolStrip.MouseHover += new System.EventHandler(this.toolStrip_MouseHover);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 42);
            // 
            // btnAudioIP
            // 
            this.btnAudioIP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAudioIP.Image = ((System.Drawing.Image)(resources.GetObject("btnAudioIP.Image")));
            this.btnAudioIP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAudioIP.Name = "btnAudioIP";
            this.btnAudioIP.Size = new System.Drawing.Size(28, 39);
            this.btnAudioIP.Text = "Audio IP Dos Vías";
            this.btnAudioIP.Click += new System.EventHandler(this.btnAudioIP_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 42);
            // 
            // btnGrabaVideo
            // 
            this.btnGrabaVideo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnGrabaVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnGrabaVideo.Image")));
            this.btnGrabaVideo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnGrabaVideo.Name = "btnGrabaVideo";
            this.btnGrabaVideo.Size = new System.Drawing.Size(28, 39);
            this.btnGrabaVideo.Text = "Grabar Video";
            this.btnGrabaVideo.Click += new System.EventHandler(this.btnGrabaVideo_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 42);
            // 
            // btnCaptura
            // 
            this.btnCaptura.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCaptura.Image = ((System.Drawing.Image)(resources.GetObject("btnCaptura.Image")));
            this.btnCaptura.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCaptura.Name = "btnCaptura";
            this.btnCaptura.Size = new System.Drawing.Size(28, 39);
            this.btnCaptura.Text = "Captura Imágen";
            this.btnCaptura.Click += new System.EventHandler(this.btnCaptura_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 42);
            // 
            // btnLogOut
            // 
            this.btnLogOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLogOut.Image = ((System.Drawing.Image)(resources.GetObject("btnLogOut.Image")));
            this.btnLogOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(28, 39);
            this.btnLogOut.Text = "Cerrar Sesión";
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 42);
            // 
            // lblEstado
            // 
            this.lblEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.ForeColor = System.Drawing.SystemColors.Info;
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(24, 39);
            this.lblEstado.Text = "....";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 42);
            // 
            // btnAgregaRCP
            // 
            this.btnAgregaRCP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnAgregaRCP.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.btnAgregaRCP.ForeColor = System.Drawing.Color.Lime;
            this.btnAgregaRCP.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregaRCP.Image")));
            this.btnAgregaRCP.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnAgregaRCP.Name = "btnAgregaRCP";
            this.btnAgregaRCP.Size = new System.Drawing.Size(123, 39);
            this.btnAgregaRCP.Text = "Añadir a RCP-Virtual";
            this.btnAgregaRCP.Visible = false;
            this.btnAgregaRCP.Click += new System.EventHandler(this.BtnAgregaRCP_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(6, 42);
            // 
            // lblEstadoCliente
            // 
            this.lblEstadoCliente.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblEstadoCliente.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblEstadoCliente.Name = "lblEstadoCliente";
            this.lblEstadoCliente.Size = new System.Drawing.Size(22, 39);
            this.lblEstadoCliente.Text = "---";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 42);
            // 
            // tmrEnLinea
            // 
            this.tmrEnLinea.Interval = 3000;
            this.tmrEnLinea.Tick += new System.EventHandler(this.tmrEnLinea_Tick);
            // 
            // tmrToolStrip
            // 
            this.tmrToolStrip.Interval = 3000;
            this.tmrToolStrip.Tick += new System.EventHandler(this.tmrToolStrip_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(105)))), ((int)(((byte)(178)))));
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(1236, 752);
            this.splitContainer1.SplitterDistance = 219;
            this.splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(105)))), ((int)(((byte)(178)))));
            this.splitContainer2.Panel1.Controls.Add(this.button1);
            this.splitContainer2.Panel1.Controls.Add(this.lblEquipo);
            this.splitContainer2.Panel1.Controls.Add(this.picLogoDisp);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.treeView);
            this.splitContainer2.Size = new System.Drawing.Size(219, 752);
            this.splitContainer2.SplitterDistance = 35;
            this.splitContainer2.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Right;
            this.button1.Location = new System.Drawing.Point(195, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 35);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // lblEquipo
            // 
            this.lblEquipo.AutoSize = true;
            this.lblEquipo.BackColor = System.Drawing.Color.Transparent;
            this.lblEquipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEquipo.ForeColor = System.Drawing.SystemColors.Info;
            this.lblEquipo.Location = new System.Drawing.Point(57, 8);
            this.lblEquipo.Name = "lblEquipo";
            this.lblEquipo.Size = new System.Drawing.Size(61, 16);
            this.lblEquipo.TabIndex = 1;
            this.lblEquipo.Text = "MARCA";
            // 
            // picLogoDisp
            // 
            this.picLogoDisp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(105)))), ((int)(((byte)(178)))));
            this.picLogoDisp.Dock = System.Windows.Forms.DockStyle.Left;
            this.picLogoDisp.Location = new System.Drawing.Point(0, 0);
            this.picLogoDisp.Name = "picLogoDisp";
            this.picLogoDisp.Size = new System.Drawing.Size(51, 35);
            this.picLogoDisp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogoDisp.TabIndex = 0;
            this.picLogoDisp.TabStop = false;
            // 
            // splitContainer3
            // 
            this.splitContainer3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.AutoScrollMargin = new System.Drawing.Size(50, 50);
            this.splitContainer3.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(1013, 752);
            this.splitContainer3.SplitterDistance = 704;
            this.splitContainer3.TabIndex = 2;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.canalZeroView);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer5.Size = new System.Drawing.Size(1013, 704);
            this.splitContainer5.SplitterDistance = 697;
            this.splitContainer5.TabIndex = 4;
            // 
            // canalZeroView
            // 
            this.canalZeroView.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.canalZeroView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.canalZeroView.Location = new System.Drawing.Point(0, 0);
            this.canalZeroView.Name = "canalZeroView";
            this.canalZeroView.Size = new System.Drawing.Size(697, 704);
            this.canalZeroView.TabIndex = 0;
            this.canalZeroView.TabStop = false;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.liveView);
            this.splitContainer6.Panel1.Controls.Add(this.textBox1);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.splitContainer7);
            this.splitContainer6.Size = new System.Drawing.Size(312, 704);
            this.splitContainer6.SplitterDistance = 329;
            this.splitContainer6.TabIndex = 5;
            // 
            // liveView
            // 
            this.liveView.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.liveView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveView.Location = new System.Drawing.Point(0, 0);
            this.liveView.Name = "liveView";
            this.liveView.Size = new System.Drawing.Size(312, 329);
            this.liveView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.liveView.TabIndex = 3;
            this.liveView.TabStop = false;
            this.liveView.Click += new System.EventHandler(this.LiveView_Click);
            this.liveView.DoubleClick += new System.EventHandler(this.LiveView_DoubleClick);
            this.liveView.MouseLeave += new System.EventHandler(this.LiveView_MouseLeave);
            this.liveView.MouseHover += new System.EventHandler(this.LiveView_MouseHover);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(152, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(131, 20);
            this.textBox1.TabIndex = 4;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox1_KeyDown_1);
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.TextBox1_KeyUp_1);
            // 
            // splitContainer7
            // 
            this.splitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer7.Location = new System.Drawing.Point(0, 0);
            this.splitContainer7.Name = "splitContainer7";
            this.splitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer7.Panel1
            // 
            this.splitContainer7.Panel1.Controls.Add(this.playbackView);
            this.splitContainer7.Panel1.Controls.Add(this.textBox2);
            // 
            // splitContainer7.Panel2
            // 
            this.splitContainer7.Panel2.Controls.Add(this.dtpHoraIni);
            this.splitContainer7.Panel2.Controls.Add(this.toolStrip1);
            this.splitContainer7.Panel2.Controls.Add(this.label1);
            this.splitContainer7.Panel2.Controls.Add(this.dttpHoraFin);
            this.splitContainer7.Panel2.Controls.Add(this.label2);
            this.splitContainer7.Size = new System.Drawing.Size(312, 371);
            this.splitContainer7.SplitterDistance = 292;
            this.splitContainer7.TabIndex = 7;
            // 
            // playbackView
            // 
            this.playbackView.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.playbackView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playbackView.Location = new System.Drawing.Point(0, 0);
            this.playbackView.Name = "playbackView";
            this.playbackView.Size = new System.Drawing.Size(312, 292);
            this.playbackView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.playbackView.TabIndex = 4;
            this.playbackView.TabStop = false;
            this.playbackView.Click += new System.EventHandler(this.PlaybackView_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(182, 17);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(130, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox2_KeyDown);
            // 
            // dtpHoraIni
            // 
            this.dtpHoraIni.CustomFormat = "HH:mm:ss";
            this.dtpHoraIni.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpHoraIni.Location = new System.Drawing.Point(98, 42);
            this.dtpHoraIni.MaxDate = new System.DateTime(2119, 7, 27, 23, 59, 0, 0);
            this.dtpHoraIni.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpHoraIni.Name = "dtpHoraIni";
            this.dtpHoraIni.ShowUpDown = true;
            this.dtpHoraIni.Size = new System.Drawing.Size(107, 20);
            this.dtpHoraIni.TabIndex = 0;
            this.dtpHoraIni.Value = new System.DateTime(2019, 7, 27, 0, 0, 0, 0);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator7,
            this.btnStop,
            this.btnPlay,
            this.btnPause,
            this.btnSlow,
            this.btnFast,
            this.toolStripSeparator8,
            this.btnCapturaPB,
            this.toolStripSeparator9,
            this.lblEnlace});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(312, 39);
            this.toolStrip1.TabIndex = 6;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 39);
            // 
            // btnStop
            // 
            this.btnStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnStop.Image = global::LiveHikVision.Properties.Resources.btnStop;
            this.btnStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(36, 36);
            this.btnStop.Text = "toolStripButton1";
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPlay.Image = global::LiveHikVision.Properties.Resources.btnPlay;
            this.btnPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(36, 36);
            this.btnPlay.Text = "toolStripButton2";
            this.btnPlay.Click += new System.EventHandler(this.BtnPlay_Click);
            // 
            // btnPause
            // 
            this.btnPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPause.Image = global::LiveHikVision.Properties.Resources.btnPause;
            this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(36, 36);
            this.btnPause.Text = "toolStripButton1";
            this.btnPause.Click += new System.EventHandler(this.BtnPause_Click);
            // 
            // btnSlow
            // 
            this.btnSlow.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSlow.Image = global::LiveHikVision.Properties.Resources.btnSlow;
            this.btnSlow.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSlow.Name = "btnSlow";
            this.btnSlow.Size = new System.Drawing.Size(36, 36);
            this.btnSlow.Text = "toolStripButton3";
            this.btnSlow.Click += new System.EventHandler(this.BtnSlow_Click);
            // 
            // btnFast
            // 
            this.btnFast.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnFast.Image = global::LiveHikVision.Properties.Resources.btnFast;
            this.btnFast.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFast.Name = "btnFast";
            this.btnFast.Size = new System.Drawing.Size(36, 36);
            this.btnFast.Text = "toolStripButton4";
            this.btnFast.Click += new System.EventHandler(this.BtnFast_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
            // 
            // btnCapturaPB
            // 
            this.btnCapturaPB.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCapturaPB.Image = global::LiveHikVision.Properties.Resources.captura1;
            this.btnCapturaPB.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCapturaPB.Name = "btnCapturaPB";
            this.btnCapturaPB.Size = new System.Drawing.Size(36, 36);
            this.btnCapturaPB.Text = "toolStripButton1";
            this.btnCapturaPB.Click += new System.EventHandler(this.BtnCapturaPB_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 39);
            // 
            // lblEnlace
            // 
            this.lblEnlace.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.lblEnlace.ForeColor = System.Drawing.SystemColors.Info;
            this.lblEnlace.Name = "lblEnlace";
            this.lblEnlace.Size = new System.Drawing.Size(92, 15);
            this.lblEnlace.Text = "toolStripLabel1";
            this.lblEnlace.Click += new System.EventHandler(this.LblEnlace_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(37, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "H. Inicio";
            // 
            // dttpHoraFin
            // 
            this.dttpHoraFin.CustomFormat = "HH:mm:ss";
            this.dttpHoraFin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dttpHoraFin.Location = new System.Drawing.Point(272, 42);
            this.dttpHoraFin.MaxDate = new System.DateTime(2219, 7, 29, 0, 0, 0, 0);
            this.dttpHoraFin.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dttpHoraFin.Name = "dttpHoraFin";
            this.dttpHoraFin.ShowUpDown = true;
            this.dttpHoraFin.Size = new System.Drawing.Size(107, 20);
            this.dttpHoraFin.TabIndex = 3;
            this.dttpHoraFin.Value = new System.DateTime(2019, 7, 27, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(222, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "H. Fin";
            // 
            // splitContainer4
            // 
            this.splitContainer4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.toolStrip);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.rbtnMain);
            this.splitContainer4.Panel2.Controls.Add(this.rbtnSub);
            this.splitContainer4.Size = new System.Drawing.Size(1013, 44);
            this.splitContainer4.SplitterDistance = 810;
            this.splitContainer4.TabIndex = 2;
            // 
            // rbtnMain
            // 
            this.rbtnMain.AutoSize = true;
            this.rbtnMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.rbtnMain.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.rbtnMain.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.rbtnMain.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rbtnMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnMain.ForeColor = System.Drawing.Color.White;
            this.rbtnMain.Location = new System.Drawing.Point(3, 23);
            this.rbtnMain.Name = "rbtnMain";
            this.rbtnMain.Size = new System.Drawing.Size(107, 19);
            this.rbtnMain.TabIndex = 3;
            this.rbtnMain.Text = "Main Stream";
            this.rbtnMain.UseVisualStyleBackColor = false;
            this.rbtnMain.CheckedChanged += new System.EventHandler(this.rbtnMain_CheckedChanged);
            // 
            // rbtnSub
            // 
            this.rbtnSub.AutoSize = true;
            this.rbtnSub.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.rbtnSub.Checked = true;
            this.rbtnSub.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.rbtnSub.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.rbtnSub.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rbtnSub.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnSub.ForeColor = System.Drawing.Color.White;
            this.rbtnSub.Location = new System.Drawing.Point(3, 3);
            this.rbtnSub.Name = "rbtnSub";
            this.rbtnSub.Size = new System.Drawing.Size(100, 19);
            this.rbtnSub.TabIndex = 4;
            this.rbtnSub.TabStop = true;
            this.rbtnSub.Text = "Sub Stream";
            this.rbtnSub.UseVisualStyleBackColor = false;
            this.rbtnSub.CheckedChanged += new System.EventHandler(this.rbtnSub_CheckedChanged);
            // 
            // btnAudio
            // 
            this.btnAudio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAudio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tstAudio1,
            this.tstAudio2});
            this.btnAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnAudio.Image")));
            this.btnAudio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAudio.Name = "btnAudio";
            this.btnAudio.Size = new System.Drawing.Size(37, 39);
            this.btnAudio.Text = "Audio Dos Vías";
            this.btnAudio.Click += new System.EventHandler(this.btnAudio_Click);
            // 
            // tstAudio1
            // 
            this.tstAudio1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.tstAudio1.CheckOnClick = true;
            this.tstAudio1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tstAudio1.Enabled = false;
            this.tstAudio1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tstAudio1.ForeColor = System.Drawing.Color.SteelBlue;
            this.tstAudio1.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.tstAudio1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstAudio1.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.tstAudio1.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.tstAudio1.Name = "tstAudio1";
            this.tstAudio1.Padding = new System.Windows.Forms.Padding(1);
            this.tstAudio1.Size = new System.Drawing.Size(182, 22);
            this.tstAudio1.Tag = "1";
            this.tstAudio1.Text = "Audio 1";
            this.tstAudio1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tstAudio1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tstAudio1.Click += new System.EventHandler(this.TstAudio1_Click);
            // 
            // tstAudio2
            // 
            this.tstAudio2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.tstAudio2.CheckOnClick = true;
            this.tstAudio2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tstAudio2.Enabled = false;
            this.tstAudio2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tstAudio2.ForeColor = System.Drawing.Color.SteelBlue;
            this.tstAudio2.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.tstAudio2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tstAudio2.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(88)))), ((int)(((byte)(89)))), ((int)(((byte)(91)))));
            this.tstAudio2.Name = "tstAudio2";
            this.tstAudio2.Padding = new System.Windows.Forms.Padding(1);
            this.tstAudio2.Size = new System.Drawing.Size(182, 22);
            this.tstAudio2.Tag = "2";
            this.tstAudio2.Text = "Audio 2";
            this.tstAudio2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.tstAudio2.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // frmLiveView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.ClientSize = new System.Drawing.Size(1236, 752);
            this.Controls.Add(this.splitContainer1);
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(740, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLiveView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SIVVIGS - ScannerExpress";
            this.TransparencyKey = System.Drawing.Color.NavajoWhite;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogoDisp)).EndInit();
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.canalZeroView)).EndInit();
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel1.PerformLayout();
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.liveView)).EndInit();
            this.splitContainer7.Panel1.ResumeLayout(false);
            this.splitContainer7.Panel1.PerformLayout();
            this.splitContainer7.Panel2.ResumeLayout(false);
            this.splitContainer7.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer7)).EndInit();
            this.splitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playbackView)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            this.splitContainer4.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Timer tmrGraba;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnGrabaVideo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnCaptura;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnLogOut;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel lblEstado;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton btnAudioIP;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.Timer tmrEnLinea;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Timer tmrToolStrip;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label lblEquipo;
        private System.Windows.Forms.PictureBox picLogoDisp;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.RadioButton rbtnMain;
        private System.Windows.Forms.RadioButton rbtnSub;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.PictureBox liveView;
        private System.Windows.Forms.PictureBox playbackView;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.DateTimePicker dttpHoraFin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpHoraIni;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton btnStop;
        private System.Windows.Forms.ToolStripButton btnPlay;
        private System.Windows.Forms.ToolStripButton btnSlow;
        private System.Windows.Forms.ToolStripButton btnFast;
        private System.Windows.Forms.ToolStripButton btnPause;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.SplitContainer splitContainer7;
        private System.Windows.Forms.PictureBox canalZeroView;
        private System.Windows.Forms.ToolStripButton btnCapturaPB;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripLabel lblEnlace;
        private System.Windows.Forms.ToolStripButton btnAgregaRCP;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripLabel lblEstadoCliente;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripDropDownButton btnAudio;
        private System.Windows.Forms.ToolStripMenuItem tstAudio1;
        private System.Windows.Forms.ToolStripMenuItem tstAudio2;
    }
}

