﻿    using System;
using System.Data;
using System.Windows.Forms;
using System.Data.Odbc;
namespace LiveHikVision
{
    class conexion
    {
        //static OdbcDataAdapter Adaptador;
        static OdbcConnectionStringBuilder cnx;
        static OdbcConnection Conexion;
        static OdbcCommand Comando;
        static OdbcDataReader Reader;
        static DataTable dt = new DataTable();

        public bool conectar()
        {
            bool estado = true;
            try
            {
                cnx = new OdbcConnectionStringBuilder();
                cnx.Dsn = "SIAC";
                Conexion = new OdbcConnection(cnx.ConnectionString);
                Conexion.Open();
            }
            catch (Exception ex)
            {
                estado = false;
                MessageBox.Show("Error al Conectar con el servidor :" + ex.ToString() + "");
            }
            return estado;
        }

        public void desconectar()
        {
            Conexion.Close();
        }

        public void obtenerDatos(string tabla, string campos, string condicion, string orden)
        {
            if (condicion != "")
            {
                condicion = "Where " + condicion;
            }
            else
            {
                condicion = "";
            }
            var SQL = "Select " + campos + " from " + tabla + " " + condicion + "" + orden + "";
            conectar();
            Comando = new OdbcCommand(SQL, Conexion);
            Reader = Comando.ExecuteReader();
            while (Reader.Read()) {
                if (Reader.GetValue(5).ToString() == "" || Reader.GetInt32(6) == 0 || Reader.GetValue(7).ToString() == "" || Reader.GetValue(8).ToString() == "") { }

                else {
                    frmLiveView.DVRIPAddress = Reader.GetValue(5).ToString();
                    frmLiveView.DVRPortNumber = Reader.GetInt32(6);
                    frmLiveView.DVRUserName = Reader.GetValue(7).ToString();
                    frmLiveView.DVRPassword = Reader.GetValue(8).ToString();
                    frmLiveView.DVREncryptKey = Reader.GetValue(9).ToString();
                    frmLiveView.tipoDispositivo = Reader.GetValue(2).ToString();
                    frmLiveView.cuentaM = Reader.GetValue(0).ToString();
                }
            }
            desconectar();
        }

        public void insertarRegistro(string tabla, string campos, string valores, string condicion)
        {
            if (condicion != "")
            {
                condicion = "Where " + condicion;
            }
            else
            {
                condicion = "";
            }
            conectar();
            var SQL = "insert " + tabla + "(" + campos + ") values(" + valores + ") " + condicion + "";
            Comando = new OdbcCommand(SQL, Conexion);
            Comando.ExecuteNonQuery();
            desconectar();
        }

        public void actualizaEdicionCliente()
        {
            conectar();
            var SQL = "Update Cliente_Monitoreo_Adt set validado = 0, UserMod= '" + Program.nomUsuario + "', DateUserMod = GETDATE() where codigo='" + Program.codigoCli + "'";
            Comando = new OdbcCommand(SQL, Conexion);
            Comando.ExecuteNonQuery();
            desconectar();
        }

        public void actualizarRegistro(string tabla, string campos, string condicion, bool tipo)
        {
            if (condicion != "")
            {
                condicion = "Where " + condicion;
            }
            else
            {
                condicion = "";
            }
            switch (tipo)
            {
                case true:
                    var SQL = "update " + tabla + " set " + campos + " " + condicion + "";
                    Comando = new OdbcCommand(SQL, Conexion);
                    Comando.ExecuteNonQuery();

                    SQL = "Update Cliente_Monitoreo_Adt set validado = 0, UserMod= '" + Program.nomUsuario + "', DateUserMod = GETDATE() where codigo='" + Program.codigoCli + "'";
                    Comando = new OdbcCommand(SQL, Conexion);
                    Comando.ExecuteNonQuery();
                    break;

                case false:
                    SQL = "Update Cliente_Monitoreo_Adt set validado = 0, UserMod= '" + Program.nomUsuario + "', DateUserMod = GETDATE() where codigo='" + Program.codigoCli + "'";
                    Comando = new OdbcCommand(SQL, Conexion);
                    Comando.ExecuteNonQuery();
                    break;
            }
        }

        public void cargaTreeWiew(TreeView trv, string tabla, string campos, string condicion, string orden)
        {
            if (condicion != "")
            {
                condicion = "Where " + condicion;
            }
            else
            {
                condicion = "";
            }
            conectar();
            var SQL = "Select " + campos + " from " + tabla + " " + condicion + "" + orden + "";
            Comando = new OdbcCommand(SQL, Conexion);
            Reader = Comando.ExecuteReader();
            while (Reader.Read())
            {
                trv.Nodes.Add(Reader["Descripcion"].ToString()).ImageIndex = 1;
            }
            desconectar();
         }

        public void habilitaCuentaRCP()
        {
            try
            {
                conectar();
                //string SQL = "UPDATE vinculacionSDI_CCTV SET listoConexion = 1 WHERE CUENTAM='" + frmLiveView.cuentaM + "' and cuentaCCTV='" + Program.codigoCli + "' ";
                string SQL = "UPDATE vinculacionSDI_CCTV SET listoConexion = 1 WHERE cuentaCCTV='" + Program.codigoCli + "' ";
                Comando = new OdbcCommand(SQL, Conexion);
                Comando.ExecuteScalar();
                desconectar();
            }catch(OdbcException err)
            {
                MessageBox.Show(err.ToString());
            }
        }

        public bool compruebaUsuario()
        {
            conectar();
            string usr = "";
            bool activa = false;
            string SQL = "select * from Usuarios where Usuario = '" + Program.nomUsuario + "'";
            Comando = new OdbcCommand(SQL, Conexion);
            Reader = Comando.ExecuteReader();
            if (Reader.Read())
            {
                usr = Reader["Permiso"].ToString();
            }
            desconectar();

            if(usr == "CALL CENTER" || usr == "TECNICO") {
                activa = true;
            }

            return activa;
        }

        public bool activoReceptora()
        {
            bool activo = false;
            string usr = "";
            conectar();
            string SQL = "select * from vinculacionSDI_CCTV where cuentaCCTV = '" + Program.codigoCli + "'";
            Comando = new OdbcCommand(SQL, Conexion);
            Reader = Comando.ExecuteReader();
            if (Reader.Read())
            {
                usr = Reader["listoConexion"].ToString();
            }
            desconectar();
            if(usr == "False")
            {
                activo = true;
            }
            else
            {
                activo = false;
            }
            return activo;
        }

        public bool estadoConexion()
        {
            bool conectado = false;
            string lCnx = "";
            string ICnx = "";
            string ECli = "";
            conectar();
            string SQL = "select * from vinculacionSDI_CCTV where cuentaCCTV = '" + Program.codigoCli + "'";
            Comando = new OdbcCommand(SQL, Conexion);
            Reader = Comando.ExecuteReader();
            if (Reader.Read())
            {
                lCnx = Reader["listoConexion"].ToString();
                ICnx = Reader["intentoConexion"].ToString();
                ECli = Reader["estadoCliente"].ToString();
            }
            desconectar();
            if (lCnx == "True" && ICnx == "True" && ECli == "1")
            {
                conectado = true;
            }
            else
            {
                conectado = false;
            }
            return conectado;
        }

        public string parametro(string var)
        {
            string param = "";
            try
            {
                string SQL = "select top(1) " + var + " from SmSiacParameters";
                Comando = new OdbcCommand(SQL, Conexion);
                Reader = Comando.ExecuteReader();
                if (Reader.Read())
                {
                    param = Reader["" + var + ""].ToString();
                }
            }
            catch (Exception r)
            {
                MessageBox.Show(r.ToString());
            }
            return param;
        }

        public void insertaImagen(byte[] img, int canal)
        {
            conectar();
            string IdInsert = parametro("idInserts");
            try
            {
                string SQL = "insert into camarasEventos(IdCamara,Cuenta,Zona,imagen, fecha, usuario) values(" +
                   "?" + ", ?" + ", ?" + ", ?" + ", ?" + ", ?" + ")";
                Comando = new OdbcCommand(SQL, Conexion);
                Comando.Parameters.Add("@id", OdbcType.NVarChar).Value = "" + IdInsert + Program.idDisp + canal + "";
                Comando.Parameters.Add("@Cuenta", OdbcType.NVarChar).Value = "" + Program.idDisp + "";
                Comando.Parameters.Add("@Zona", OdbcType.Int).Value = "" + canal + "";
                Comando.Parameters.Add("@imagen", OdbcType.VarBinary).Value = img;
                Comando.Parameters.Add("@fecha", OdbcType.DateTime).Value = "" + DateTime.Now.ToString() + "";
                Comando.Parameters.Add("@usuario", OdbcType.NVarChar).Value = "" + Program.nomUsuario + "";
                Comando.ExecuteNonQuery();
            }
            catch (Exception ERR)
            {
                MessageBox.Show("No se pudo Guardar la Imagen " + ERR);
            }
            desconectar();
        }

        public void insertaRutaImagen(string ruta, int canal)
        {
            conectar();
            string IdInsert = parametro("idInserts");
            try
            {
                string SQL = "insert into camarasEventos(IdCamara,Cuenta,Zona,imagen, fecha, usuario) values('" + IdInsert + Program.idDisp + canal + "','" + Program.idDisp + "', '" + canal + "', '" + ruta + "', '" + DateTime.Now.ToString() + "', '" + Program.nomUsuario + "')";
                Comando = new OdbcCommand(SQL, Conexion);
                Comando.ExecuteScalar();
            }
            catch (OdbcException err)
            {
                MessageBox.Show("No se pudo Guardar la Imagen " + err);
            }
            desconectar();
        }
    }
}
