﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;


namespace LiveHikVision
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        /// 
        public static string codigoCli;
        public static string zonaCLi;
        public static string nomUsuario = "";
        public static string idDisp;
        public static string idServicio;

        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                //MessageBox.Show("Hola " + args[1].ToString() + "/" + args[2].ToString() + "/" + args[3].ToString() + "/" + args[4].ToString() + "/" + args[5].ToString());
                codigoCli = args[1].ToString();
                zonaCLi = args[2].ToString();
                nomUsuario = args[3].ToString();
                idDisp = args[4].ToString();
                idServicio = args[5].ToString();
            }


            var me = Process.GetCurrentProcess();
            var arrProcesses = Process.GetProcessesByName(me.ProcessName);
            try
            {
                if (arrProcesses.Length > 3)
                {
                    Process[] procesos = Process.GetProcessesByName(me.ProcessName);
                    procesos[0].Kill();
                }
            }catch(Exception err)
            {
                var error = err.ToString();   
            }
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);
            Application.Run(new frmLiveView());
            //Application.Run(new mainForm());
        }
    }
}
