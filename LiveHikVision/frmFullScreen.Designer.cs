﻿namespace LiveHikVision
{
    partial class frmFullScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.liveView = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.liveView)).BeginInit();
            this.SuspendLayout();
            // 
            // liveView
            // 
            this.liveView.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.liveView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.liveView.Location = new System.Drawing.Point(0, 0);
            this.liveView.Name = "liveView";
            this.liveView.Size = new System.Drawing.Size(800, 450);
            this.liveView.TabIndex = 0;
            this.liveView.TabStop = false;
            this.liveView.DoubleClick += new System.EventHandler(this.LiveView_DoubleClick);
            // 
            // frmFullScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.liveView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmFullScreen";
            this.Text = "frmFullScreen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmFullScreen_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmFullScreen_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.liveView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox liveView;
    }
}