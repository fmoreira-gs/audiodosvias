﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace LiveHikVision
{
    public partial class frmLiveView : Form
    {
        conexion conn = new conexion();
        #region VARIALBES PUBLICAS
        public static string DVRIPAddress = "";
        public static Int32 DVRPortNumber = 0;
        public static string DVRUserName = "";
        public static string DVRPassword = "";
        public static string DVREncryptKey = "";
        public static string tipoDispositivo = "";
        public static string cuentaM = "";
        public static int canal;
        public static Int32 m_lUserID = -1;
        public Int32 m_lRealHandle = -1;

        public bool m_bInitSDK = false;
        CHCNetSDK.NET_DVR_USER_LOGIN_INFO struLoginInfo = new CHCNetSDK.NET_DVR_USER_LOGIN_INFO();
        CHCNetSDK.NET_DVR_DEVICEINFO_V40 struDeviceInfoV40 = new CHCNetSDK.NET_DVR_DEVICEINFO_V40();
        public static CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
        public static CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO();
        public uint tipoStream;
        public Int32 m_lRealZero = -1;
        public Int32 m_playBack = -1;
        public bool enLinea = false;
        public string str;
        //public string _iconos = System.IO.Path.Combine(Application.StartupPath, @"\Resources\img\");
        public string _iconos = Application.StartupPath + @"\Resources\img\";
        public string _capturas = "";
        public string _videos = "";
        #endregion VARIALBES PUBLICAS

        #region VARIALBES PRIVADAS
        //VARIABLES LOCALES
        private uint byStartDTalkChan = 1;
        private Int32 iLastErr = 0;
        private long m_lVoiceHanle;

        private uint m_AysnLoginResult = 0;
        private int m_iUserID = -1;
        private bool LoginCallBackFlag = false;

        bool audioAct = false;
        bool m_bRecord = false;
        int canalVideo;
        int canalAudio;
        int canalIPVideo;
        int canalIPAudio;
        int maxIPChannels;
        int maxChannels;
        uint dwVoiceChanel = 0;
        uint dwVoiceIPChanel = 0;
        uint dwAudioChanNum = 0;
        bool realPlayError = false;
        uint velocidadPTZ = 6;
        #endregion VARIALBES PRIVADAS

        public frmLiveView()
        {
            //Program.codigoCli = "G002";
            //Program.nomUsuario = "FMOREIRA";
            //Program.zonaCLi = "A1";
            //Program.idDisp = "G002";
            //Program.idServicio = "-116";
            InitializeComponent();
            m_bInitSDK = CHCNetSDK.NET_DVR_Init();
            if (m_bInitSDK == false)
            {
                lblEstado.Text = "Error al iniciar el SDK";
                return;
            }
            else
            {
                //MessageBox.Show(procesoActualID.Id.ToString());
                //To save the SDK log
                CHCNetSDK.NET_DVR_SetLogToFile(3, "C:\\SdkLog\\", true);
                CHCNetSDK.NET_DVR_SetConnectTime(2000, 1);
                CHCNetSDK.NET_DVR_SetReconnect(10000, true);
            }
        }

        private void obtenerCanal(string cadena)
        {
            string[] partes = cadena.Split(' ');
            string subcadena = partes[0];
            int numCaracteres = subcadena.Length;
            int num;
            if (partes[0] == "Z1")
            {
                /*m_lRealZero = -1;
                canalZero();*/
                return;
            }
            else
            {
                num = Convert.ToInt32(subcadena.Substring(1, subcadena.Length - 1));
                if (subcadena.Substring(0, 1) == "A")
                {
                    ///CANAL VIDEO
                    if (num <= maxChannels)
                    {
                        lblEstado.Text = "Vista en directo Canal Analogo (" + num + ")";
                        canalVideo = (num);
                        vistaEnDirecto(Convert.ToInt32(canalVideo), tipoStream);
                        btnAudioIP.Enabled = false;
                        realPlayError = false;
                    }
                    else
                    {
                        lblEstado.Text = "El Canal (" + num + ") es mayor al Permitido (" + maxChannels + ")";
                        realPlayError = true;
                        return;
                    }
                    ///CANAL AUDIO
                    if (num <= dwAudioChanNum){
                        canalAudio = num;
                        if (DeviceInfo.byAudioChanNum == 0)
                        {
                            btnAudio.Enabled = false;
                        }
                        else
                        {
                            btnAudio.Enabled = true;
                        }
                    }
                    else
                    {
                        canalAudio = Convert.ToInt32(dwAudioChanNum);
                    }
                    dwVoiceChanel = Convert.ToUInt32(canalAudio);
                    canal = canalVideo;
                }
                else if (subcadena.Substring(0, 1) == "D" && Convert.ToUInt32(tipoDispositivo) != 2)
                {
                    byStartDTalkChan = DeviceInfo.byStartDTalkChan + Convert.ToUInt32(2);
                    ///CANAL VIDEO IP
                    if (DeviceInfo.byIPChanNum == 0)
                    {
                        lblEstado.Text = "El dispositivo no tiene Canal IP";
                    }
                    else
                    {
                        int numIP = num + 32;
                        //SE HABILITA EL NUMERO DE CANAL PARA VIDEO DIGITAL
                        canalIPVideo = numIP;
                        if (num <= maxIPChannels)
                        {
                            lblEstado.Text = "Vista en directo Canal Digital(IP) (" + num + ")";
                            vistaEnDirecto(Convert.ToInt32(canalIPVideo), tipoStream);
                            realPlayError = false;
                        }
                        else
                        {
                            lblEstado.Text = "El Canal IP (" + canalVideo + ") es mayor al Permitido (" + maxIPChannels + ")";
                            realPlayError = true;
                            return;
                        }
                    }
                    ///CANAL AUDIO IP
                    canalIPAudio = (Convert.ToInt32(dwAudioChanNum) + num);
                    if (canalIPAudio >= byStartDTalkChan)
                    {
                        btnAudioIP.Enabled = true;
                    }
                    else
                    {
                        btnAudioIP.Enabled = false;
                    }
                    dwVoiceIPChanel = Convert.ToUInt32(canalIPAudio);
                    canal = canalIPVideo;
                }
                else if (subcadena.Substring(0, 1) == "D" && Convert.ToUInt32(tipoDispositivo) == 2)
                {
                    ///CANAL VIDEO
                    btnAudio.Enabled = false;
                    if (num <= maxChannels)
                    {
                        lblEstado.Text = "Vista en directo Cámara IP ";
                        canalVideo = (num);
                        vistaEnDirecto(Convert.ToInt32(canalVideo), tipoStream);
                        btnAudioIP.Enabled = false;
                        realPlayError = false;
                    }
                    else
                    {
                        lblEstado.Text = "El Canal (" + num + ") es mayor al Permitido (" + maxChannels + ")";
                        realPlayError = true;
                        return;
                    }
                    ///CANAL AUDIO
                    if (num <= dwVoiceChanel)
                    {
                        canalAudio = (num);
                        if (DeviceInfo.byAudioChanNum == 0)
                        {
                            btnAudioIP.Enabled = true;
                        }
                        else
                        {
                            btnAudioIP.Enabled = true;
                        }
                    }
                    else
                    {
                        canalAudio = Convert.ToInt32(dwVoiceChanel);
                    }
                    dwVoiceChanel = Convert.ToUInt32(canalAudio);
                    canal = canalVideo;
                }
            }
        }

        public void AsynLoginMsgCallback(Int32 lUserID, UInt32 dwResult, IntPtr lpDeviceInfo, IntPtr pUser)
        {
            if (dwResult == 1)
            {

                DeviceInfo = (CHCNetSDK.NET_DVR_DEVICEINFO_V30)Marshal.PtrToStructure(lpDeviceInfo, typeof(CHCNetSDK.NET_DVR_DEVICEINFO_V30));
            }

            m_AysnLoginResult = dwResult;
            m_iUserID = lUserID;
            LoginCallBackFlag = true;
            if (!LoginCallBackFlag)
            {
                MessageBox.Show("Asynchronous login callback time out!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblEnlace.Text = "";
            tipoVistaDirecto();

            if (conn.compruebaUsuario())
            {
                button1.Text = ">";
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                button1.Text = "<";
                inicioForm();
            }

            if (m_lUserID < 0)
            {
                if (Program.codigoCli == "" || Program.nomUsuario == "" || Program.zonaCLi == "")
                {
                    lblEstado.Text = "No hay Datos para conexion..!!!";
                    return;
                }

                if (Program.idServicio == "-116")
                {
                    conn.obtenerDatos("vinculacionSDI_CCTV", "*", " cuentaCCTV = '" + Program.codigoCli + "'", "");
                }
                else
                {
                    if (Program.codigoCli == Program.idDisp)
                    {
                        conn.obtenerDatos("vinculacionSDI_CCTV", "*", " cuentaM = '" + Program.codigoCli + "' AND cuentaCCTV = ''", "");
                    }
                    else
                    {
                        conn.obtenerDatos("vinculacionSDI_CCTV", "*", " cuentaM = '" + Program.codigoCli + "' AND cuentaCCTV = '" + Program.idDisp + "'", "");
                    }
                }

                struLoginInfo.bUseAsynLogin = false;
                struLoginInfo.cbLoginResult = new CHCNetSDK.LOGINRESULTCALLBACK(AsynLoginMsgCallback);
                struLoginInfo.sDeviceAddress = DVRIPAddress;
                struLoginInfo.wPort = Convert.ToChar(DVRPortNumber);
                struLoginInfo.sUserName = DVRUserName;
                struLoginInfo.sPassword = DVRPassword;
                if (DVRIPAddress.Trim() == "" || DVRPortNumber == 0 || DVRUserName.Trim() == "" || DVRPassword.Trim() == "")
                {
                    lblEstado.Text = "El Cliente no Tiene Registrado Ningun dispositivo para monitoreo";
                }
                else
                {
                    m_lUserID = CHCNetSDK.NET_DVR_Login_V40(ref struLoginInfo, ref struDeviceInfoV40);
                    DeviceInfo = struDeviceInfoV40.struDeviceV30;
                    dwAudioChanNum = DeviceInfo.byAudioChanNum;
                    maxIPChannels = DeviceInfo.byIPChanNum;
                    maxChannels = DeviceInfo.byChanNum;
                    if(dwAudioChanNum > 0)
                    {
                        dwVoiceChanel = 1;
                        btnAudio.Enabled = true;
                        tstAudio1.Enabled = true;
                    }
                    else
                    {
                        btnAudio.Enabled = false;
                    }

                    if (m_lUserID < 0)
                    {
                        iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                        str = "NET_DVR_Login_V30 failed, error code= " + iLastErr; //µÇÂ¼Ê§°Ü£¬Êä³ö´íÎóºÅ
                        lblEstado.Text = str + "Error al inicial Sesion...";
                        btnAgregaRCP.Visible = false;
                        return;
                    }
                    else
                    {
                        //µÇÂ¼³É¹¦ 
                        lblEstado.Text = "Login Correcto - CONECTADO...";
                        igualaHora();

                        if (conn.compruebaUsuario())
                        {
                            btnAgregaRCP.Visible = true;
                            if (conn.activoReceptora())
                            {
                                btnAgregaRCP.Enabled = true;
                            }
                            else
                            {
                                btnAgregaRCP.Enabled = false;
                                lblEstadoCliente.Text = "Cliente Registrado en la Receptora Virtua.!";
                            }
                        }
                    }
                    lblEquipo.Text = "DVR-HIK-" + Program.idDisp.ToUpper() + "\n";
                    picLogoDisp.Image = Image.FromFile(_iconos + "dvr.png");
                }
                conn.cargaTreeWiew(treeView, "CamarasCuentas", "zona,tipoCanal + Cast(zona as nvarchar) + ' - ' + Descripcion as Descripcion", " cuenta = '" + Program.idDisp + "'", " Order by tipoCanal, zona");
                obtenerCanal(Program.zonaCLi);
            }
            if(this.WindowState == FormWindowState.Maximized)
            {
                canalZero();
            }
        }

        private void igualaHora()
        {
            DateTime ahora = DateTime.Now;
            CHCNetSDK.NET_DVR_TIME horaActual;
            horaActual.dwYear = (uint)ahora.Year;
            horaActual.dwMonth = (uint)ahora.Month;
            horaActual.dwDay = (uint)ahora.Day;
            horaActual.dwHour = (uint)ahora.Hour;
            horaActual.dwMinute = (uint)ahora.Minute;
            horaActual.dwSecond = (uint)ahora.Second;

            //SE DEVUELVE EL TAMAÑO EL OBJETO DE HORA CONVERTIDO EN A BYTES
            int dwSize = Marshal.SizeOf(horaActual);
            //ASIGNO MEMORIA AL PUNTERO
            IntPtr ptrCurTime = Marshal.AllocHGlobal(dwSize);

            Marshal.StructureToPtr(horaActual, ptrCurTime, false);

            if (!CHCNetSDK.NET_DVR_SetDVRConfig((int)m_lUserID, CHCNetSDK.NET_DVR_SET_TIMECFG, 0, ptrCurTime, (uint)dwSize))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "Hubo un Error al Sincronicar la Hora, error code= " + iLastErr; //µÇÂ¼Ê§°Ü£¬Êä³ö´íÎóºÅ
                lblEstado.Text = str;
            }
            //SE LIBERA LA MEMORIA PREVIAMENTE ASIGNADA
            Marshal.FreeHGlobal(ptrCurTime);
        }

        public void RealDataCallBack(Int32 lRealHandle, UInt32 dwDataType, ref byte pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {

        }

        public void vistaEnDirecto(int canal, uint tipoStream)
        {
            //INSERTO LA CLAVE DE CIFRADO AL SDK PARA LA TRANSMISION DE VIDEO
            bool claveEncryptacion = CHCNetSDK.NET_DVR_SetSDKSecretKey(m_lUserID, DVREncryptKey);
            Boolean BENCRYPT = new Boolean();
            bool ESTADO = CHCNetSDK.NET_DVR_InquestGetEncryptState(m_lUserID, canal, BENCRYPT);

            //-------------------
            //CHCNetSDK.NET_DVR_ZeroStopPlay(m_lRealZero);
            //m_lRealZero = -1;
            if (m_lRealHandle >= 0)
            {
                //Í£Ö¹Ô¤ÀÀ Stop live view 
                if (!CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_StopRealPlay failed, error code= " + iLastErr;
                    lblEstado.Text = str;
                }
                m_lRealHandle = -1;
            }
            else
            {
                if (m_lUserID < 0)
                {
                    lblEstado.Text = "Por Favor Primero hacer logina al Dispositivo";
                    return;
                }
            }

            lpPreviewInfo.hPlayWnd = liveView.Handle;//Ô¤ÀÀ´°¿Ú
            lpPreviewInfo.lChannel = Convert.ToInt32(canal.ToString());//Ô¤teÀÀµÄÉè±¸Í¨µÀ
            lpPreviewInfo.dwStreamType = tipoStream;//ÂëÁ÷ÀàÐÍ£º0-Ö÷ÂëÁ÷£¬1-×ÓÂëÁ÷£¬2-ÂëÁ÷3£¬3-ÂëÁ÷4£¬ÒÔ´ËÀàÍÆ
            lpPreviewInfo.dwLinkMode = 0;//Á¬½Ó·½Ê½£º0- TCP·½Ê½£¬1- UDP·½Ê½£¬2- ¶à²¥·½Ê½£¬3- RTP·½Ê½£¬4-RTP/RTSP£¬5-RSTP/HTTP 
            lpPreviewInfo.bBlocked = false; //0- ·Ç×èÈûÈ¡Á÷£¬1- ×èÈûÈ¡Á÷
            lpPreviewInfo.byPreviewMode = 0; //Modo de vista previa: 0- vista previa normal, 1 vista previa de retraso
            CHCNetSDK.REALDATACALLBACK RealData = new CHCNetSDK.REALDATACALLBACK(RealDataCallBack);//Ô¤ÀÀÊµÊ±Á÷»Øµ÷º¯Êý
            IntPtr pUser = new IntPtr();//ÓÃ»§Êý¾Ý, ´ò¿ªÔ¤ÀÀ Start live view 
            m_lRealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref lpPreviewInfo, null /* RealData*/, pUser);
            if (m_lRealHandle < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr; //Ô¤ÀÀÊ§°Ü£¬Êä³ö´íÎóºÅ
                lblEstado.Text = str;
                return;
            }

            vistaPlayback(canal, false);
        }

        private void tipoVistaDirecto()
        {
            if (rbtnSub.Checked == true)
            {
                tipoStream = 1;
            }
            if (rbtnMain.Checked == true)
            {
                tipoStream = 0;
            }
        }

        private void canalZero()
        {
            IntPtr pUser = new IntPtr();//ÓÃ»§Êý¾Ý
            CHCNetSDK.NET_DVR_CLIENTINFO lpClientInfo = new CHCNetSDK.NET_DVR_CLIENTINFO();
            lpClientInfo.lChannel = 1;
            lpClientInfo.lLinkMode = 0;
            lpClientInfo.hPlayWnd = canalZeroView.Handle;
            lpClientInfo.sMultiCastIP = null;

            CHCNetSDK.NET_DVR_MakeKeyFrame(m_lUserID, lpClientInfo.lChannel);
            if (m_lRealZero < 0)
            {
                m_lRealZero = CHCNetSDK.NET_DVR_ZeroStartPlay(m_lUserID, ref lpClientInfo, null, pUser, true);
                if (m_lRealZero < 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_ZeroStartPlay failed, error code= " + iLastErr;
                    lblEstado.Text = str;
                    liveView.Image = null;
                    return;
                }
                else
                {
                    lblEstado.Text = "Canal Zero";
                    btnAudio.Enabled = false;
                    btnAudioIP.Enabled = false;
                }
            }
        }

        private void treeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            tipoVistaDirecto();
            btnAudio.Enabled = true;
            string texto = e.Node.Text.ToString();
            if (texto == "Z1 - CANAL ZERO")
            {
                obtenerCanal(texto);
                rbtnMain.Enabled = false;
                rbtnSub.Enabled = false;
                return;
            }
            obtenerCanal(texto);
            rbtnMain.Enabled = true;
            rbtnSub.Enabled = true;
            if (realPlayError)
            {
                CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle);
                liveView.Image = null;
            }
        }

        private void fVoiceDataCallBack(int lVoiceComHandle, string pRecvDataBuffer, uint dwBufSize, byte byAudioFlag, System.IntPtr pUser)
        {
        }

        private void tmrGraba_Tick(object sender, EventArgs e)
        {
            btnGrabaVideo.PerformClick();
            tmrGraba.Enabled = false;
        }

        private void accionaAudio(int tipo)
        {
            string msg = "";
            CHCNetSDK.VOICEDATACALLBACKV30 RealAudio = new CHCNetSDK.VOICEDATACALLBACKV30(fVoiceDataCallBack);
            IntPtr pUser = new IntPtr(); //ÓÃ»§Êý¾Ý
            if (audioAct == false)
            {
                if (tipo == 0)
                {
                    m_lVoiceHanle = CHCNetSDK.NET_DVR_StartVoiceCom_V30(m_lUserID, dwVoiceChanel, false, null/*RealAudio*/, pUser);
                    msg = "Audio HABILITADO";
                }
                else if (tipo == 1)
                {
                    m_lVoiceHanle = CHCNetSDK.NET_DVR_StartVoiceCom_V30(m_lUserID, dwVoiceIPChanel, false, null/*RealAudio*/, pUser);
                    msg = "Audio IP HABILITADO";
                }
                if (m_lVoiceHanle < 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_NET_DVR_StartVoiceCom_V30 failed, error code= " + iLastErr;
                    lblEstado.Text = str;
                }
                else
                {
                    audioAct = true;
                    if (tipo == 0)
                    {
                        btnAudio.Image = Image.FromFile(_iconos + "micActivo.png");
                    }
                    else
                    {
                        btnAudioIP.Image = Image.FromFile(_iconos + "micIP-ON.png");
                    }
                    lblEstado.Text = msg;
                }
            }
            else if (audioAct == true)
            {
                if (!CHCNetSDK.NET_DVR_StopVoiceCom(Convert.ToInt32(m_lVoiceHanle)))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_NET_DVR_StartVoiceCom_V30 failed, error code= " + iLastErr;
                    lblEstado.Text = str;
                }
                else
                {
                    audioAct = false;
                    if (tipo == 0)
                    {
                        btnAudio.Image = Image.FromFile(_iconos + "micMute.png");
                    }
                    else
                    {
                        btnAudioIP.Image = Image.FromFile(_iconos + "micIP-OFF.png");
                    }
                    lblEstado.Text = "Conectado...";
                }
            }
        }

        private void btnAudio_Click(object sender, EventArgs e)
        {
           // accionaAudio(0);
        }

        private void btnGrabaVideo_Click(object sender, EventArgs e)
        {
            //Â¼Ïñ±£´æÂ·¾¶ºÍÎÄ¼þÃû the path and file name to save
            string sVideoFileName;
            _videos = @"\\192.168.3.105\Media\video\" + Program.idDisp + canal + @"\";
            string nombre = DateTime.Now.ToLocalTime().ToString().Replace(":", "");
            nombre = nombre.Replace("/", "").Trim();
            sVideoFileName = _videos + "VID_" + nombre + ".mp4";

            if (!Directory.Exists(_videos))
            {
                Directory.CreateDirectory(_videos);
            }

            if (m_bRecord == false)
            {
                //Ç¿ÖÆIÖ¡ Make a I frame
                int lChannel = canal; //Í¨µÀºÅ Channel number
                CHCNetSDK.NET_DVR_MakeKeyFrame(m_lUserID, lChannel);
                //¿ªÊ¼Â¼Ïñ Start recording
                if (!CHCNetSDK.NET_DVR_SaveRealData(m_lRealHandle, sVideoFileName))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_SaveRealData failed, error code= " + iLastErr;
                    lblEstado.Text = str;
                    return;
                }
                else
                {
                    btnGrabaVideo.Image = Image.FromFile(_iconos + "grabar-rojo.png");
                    m_bRecord = true;
                    tmrGraba.Enabled = true;
                    lblEstado.Text = "Conectado - Grabando Video";
                }
            }
            else
            {
                //Í£Ö¹Â¼Ïñ Stop recording
                if (!CHCNetSDK.NET_DVR_StopSaveRealData(m_lRealHandle))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_StopSaveRealData failed, error code= " + iLastErr;
                    //MessageBox.Show(str);
                    lblEstado.Text = str;
                    return;
                }
                else
                {
                    //str = "Successful to stop recording and the saved file is " + sVideoFileName;
                    btnGrabaVideo.Image = Image.FromFile(_iconos + "grabar-negro.png");
                    m_bRecord = false;
                    tmrGraba.Enabled = false;
                    lblEstado.Text = "Conectado....";
                }
            }
        }

        private void btnCaptura_Click(object sender, EventArgs e)
        {
            string sJpegPicFileName;
            _capturas = @"\\192.168.3.123\cam_evt\imagenes\" + Program.idDisp + canal + @"\";
            
            //Í¼Æ¬±£´æÂ·¾¶ºÍÎÄ¼þÃû the path and file name to save
            string nombre = DateTime.Now.ToLocalTime().ToString().Replace(":", "");
            nombre = nombre.Replace("/", "").Trim();
            if (!Directory.Exists(_capturas))
            {
                Directory.CreateDirectory(_capturas);
            }
            sJpegPicFileName = _capturas + "IMG_" + nombre + ".jpeg";
            int lChannel = canal; //Í¨µÀºÅ Channel number

            CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
            lpJpegPara.wPicQuality = 0; //0; //Í¼ÏñÖÊÁ¿ Image quality
            lpJpegPara.wPicSize = 2;//0xff; //×¥Í¼·Ö±æÂÊ Picture size: 2- 4CIF£¬0xff- Auto(Ê¹ÓÃµ±Ç°ÂëÁ÷·Ö±æÂÊ)£¬×¥Í¼·Ö±æÂÊÐèÒªÉè±¸Ö§³Ö£¬¸ü¶àÈ¡ÖµÇë²Î¿¼SDKÎÄµµ
            if (canal != 0)
            {
                //JPEG×¥Í¼ Capture a JPEG picture
                if (!CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, sJpegPicFileName))
                {
                    if (!CHCNetSDK.NET_DVR_CapturePictureBlock(m_lRealHandle, sJpegPicFileName, 1))
                    {
                        iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                        str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                        lblEstado.Text = str;
                        return;
                    }
                    else
                    {
                        str = "La Captura se realizó correctamente..!";
                        //MessageBox.Show(str);
                        lblEstado.Text = str;
                        //byte[] img = File.ReadAllBytes(sJpegPicFileName);
                        try
                        {
                            //SE ENVIA A GUARDAR LA IMAGEN CAPTURADA A LA BASE DE DATOS PARA HISTORIAL DE IMAGENES A EL CODIGO ACTIVO
                            //conn.insertaImagen(img, canal);
                            conn.insertaRutaImagen(sJpegPicFileName, canal);
                            //LUEGO DE GUARDAR LA IMAGEN EN LA BDD SE LA ELIMINA
                            //File.Delete(sJpegPicFileName);
                        }
                        catch (Exception) { str = "Hubo un problema al Almacenar la Captura en la Base de Datos..!"; }
                    }
                }
                else
                {
                    str = "La Captura se realizó correctamente..!";
                    //MessageBox.Show(str);
                    lblEstado.Text = str;

                    //NUEVO METODO SIMPLIFICADO DE EXTRAER BYTES DE IMAGEN IMPLEMENTADO 14-03-2019
                    //byte[] img = File.ReadAllBytes(sJpegPicFileName);
                    try
                    {
                        //SE ENVIA A GUARDAR LA IMAGEN CAPTURADA A LA BASE DE DATOS PARA HISTORIAL DE IMAGENES A EL CODIGO ACTIVO
                        conn.insertaRutaImagen(sJpegPicFileName, canal);
                        //conn.insertaImagen(img, canal);
                        //LUEGO DE GUARDAR LA IMAGEN EN LA BDD SE LA ELIMINA
                       // File.Delete(sJpegPicFileName);
                    }
                    catch (Exception) { str = "Hubo un problema al Almacenar la Captura en la Base de Datos..!"; }
                }
            }
            else
            {
                lblEstado.Text = "El número de canal no es el Correcto..!!";
            }
            return;
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            //×¢ÏúµÇÂ¼ Logout the device
            if (audioAct)
            {
                lblEstado.Text = "Por Favor Desactive el audio Primero";
                return;
            }

            m_playBack = -1;

            if (!CHCNetSDK.NET_DVR_Logout(m_lUserID))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_Logout failed, error code= " + iLastErr;
                lblEstado.Text = str;
                return;
            }
            else
            {
                m_bInitSDK = CHCNetSDK.NET_DVR_Cleanup();
            }
            m_lUserID = -1;
            m_lRealHandle = -1;
            audioAct = false;
            this.Close();
        }

        private void btnAudioIP_Click(object sender, EventArgs e)
        {
            accionaAudio(1);
        }

        /// <summary>
        /// Evento con el cual se pretende monitorear si está en línea o desconectado el dispositivo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void tmrEnLinea_Tick(object sender, EventArgs e)
        {
            bool conec = CHCNetSDK.NET_DVR_SetReconnect(1000, true);
            if (conec)
            {
                lblEstado.Text = "DISPOSITIVO EN LINEA";
            }
            else
            {
                lblEstado.Text = "DISPOSITIVO DESCONECTADO";
            }
        }

        private void toolStrip_MouseHover(object sender, EventArgs e)
        {
            tmrToolStrip.Stop();
            toolStrip.Visible = true;
        }

        private void tmrToolStrip_Tick(object sender, EventArgs e)
        {
            //toolStrip.Visible = false;
        }

        private void toolStrip_MouseLeave(object sender, EventArgs e)
        {
            tmrToolStrip.Start();
        }

        private void treeView_AfterCheck(object sender, TreeViewEventArgs e)
        {
            //treeView.SelectedNode.Checked = true;
            if (e.Node.Checked == true)
            {
                e.Node.ImageIndex = 2;
            }
            else
            {
                e.Node.ImageIndex = 1;
            }
        }

        private void rbtnSub_CheckedChanged(object sender, EventArgs e)
        {
            tipoVistaDirecto();
            vistaEnDirecto(canal, tipoStream);
        }

        private void rbtnMain_CheckedChanged(object sender, EventArgs e)
        {
            tipoVistaDirecto();
            vistaEnDirecto(canal, tipoStream);
        }

        /// <summary>
        /// FUNCIONES PARA EL CONTROL DE LAS CAMARA PTZ DE CLIENTES
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region ControlPTZ

        private void LiveView_Click(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void LiveView_MouseHover(object sender, EventArgs e)
        {
            tmrToolStrip.Stop();
            toolStrip.Visible = true;
        }

        private void LiveView_MouseLeave(object sender, EventArgs e)
        {
            tmrToolStrip.Start();
        }

        private void TextBox1_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Right)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_RIGHT, 0, velocidadPTZ);
                // MessageBox.Show("Presionò derecha");
            }
            else if (e.KeyData == Keys.Left)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_LEFT, 0, velocidadPTZ);
                // MessageBox.Show("Presionò Izquierda");
            }
            else if (e.KeyData == Keys.Down)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_DOWN, 0, velocidadPTZ);
                // MessageBox.Show("Presionó Abajo");
            }
            else if (e.KeyData == Keys.Up)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_UP, 0, velocidadPTZ);
                //MessageBox.Show("Presionó arriba");
            }
            else if (e.KeyData == Keys.D1)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_OUT, 0, velocidadPTZ);
                //MessageBox.Show("Presionó 1");
            }
            else if (e.KeyData == Keys.D2)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_IN, 0, velocidadPTZ);
                //MessageBox.Show("Presionó 2");
            }

        }

        private void TextBox1_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Right)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_RIGHT, 1, velocidadPTZ);
            }
            else if (e.KeyData == Keys.Left)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_LEFT, 1, velocidadPTZ);
            }
            else if (e.KeyData == Keys.Down)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_DOWN, 1, velocidadPTZ);
            }
            else if (e.KeyData == Keys.Up)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_UP, 1, velocidadPTZ);
            }
            else if (e.KeyData == Keys.D1)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_OUT, 1, velocidadPTZ);
            }
            else if (e.KeyData == Keys.D2)
            {
                CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_IN, 1, velocidadPTZ);
            }
        }
        #endregion

        #region PLAYBACK
        private bool enPausa = false;
        public void vistaPlayback(int canal, bool tipo)
        {
            splitContainer7.Panel1.Refresh();
            DateTime horaIni;
            DateTime ahora;
            stopPlayBack();
            if (tipo == false)
            {
                horaIni = DateTime.Now.AddMinutes(-2);
                ahora = DateTime.Now;
            }
            else
            {
                horaIni = dtpHoraIni.Value;
                ahora = dttpHoraFin.Value;
            }

            dtpHoraIni.Value = horaIni;

            CHCNetSDK.NET_DVR_TIME inicio = new CHCNetSDK.NET_DVR_TIME();
            inicio.dwYear = Convert.ToUInt32(horaIni.Year);
            inicio.dwMonth = Convert.ToUInt32(horaIni.Month);
            inicio.dwDay = Convert.ToUInt32(horaIni.Day);
            inicio.dwHour = Convert.ToUInt32(horaIni.Hour);
            inicio.dwMinute = Convert.ToUInt32(horaIni.Minute);
            inicio.dwSecond = Convert.ToUInt32(horaIni.Second);

            dttpHoraFin.Value = ahora;

            CHCNetSDK.NET_DVR_TIME fin = new CHCNetSDK.NET_DVR_TIME();
            fin.dwYear = Convert.ToUInt32(ahora.Year);
            fin.dwMonth = Convert.ToUInt32(ahora.Month);
            fin.dwDay = Convert.ToUInt32(ahora.Day);
            fin.dwHour = Convert.ToUInt32(ahora.Hour);
            fin.dwMinute = Convert.ToUInt32(ahora.Minute);
            fin.dwSecond = Convert.ToUInt32(ahora.Second);

            m_playBack = CHCNetSDK.NET_DVR_PlayBackByTime(m_lUserID, canal, ref inicio, ref fin, playbackView.Handle);
            if (m_playBack < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PlayBackByTime failed, error code= " + iLastErr;
                lblEstado.Text = str;
                playbackView.Image = null;
                return;
            }

            uint lopValue = 0;
            bool InicioPlayback = CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYSTART, 0, ref lopValue);
            if (InicioPlayback == false)
            {

                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PlayBackByControl failed, error code= " + iLastErr;
                splitContainer7.Panel1.Refresh();
                lblEstado.Text = str;
                return;
            }
        }

        public void stopPlayBack()
        {
            if (m_playBack >= 0)
            {
                splitContainer7.Panel1.Refresh();
                CHCNetSDK.NET_DVR_StopPlayBack(m_playBack);
                m_playBack = -1;
            }
        }

        private void PlaybackView_Click(object sender, EventArgs e)
        {
            textBox2.Focus();
        }

        private void TextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            uint lopValue = 0;
            if (e.KeyData == Keys.Right)
            {
                bool InicioPlayback = CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYFAST, 0, ref lopValue);
            }
            else if (e.KeyData == Keys.Left)
            {
                bool InicioPlayback = CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYSLOW, 0, ref lopValue);
            }
            else if (e.KeyData == Keys.Down)
            {
                bool InicioPlayback = CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYNORMAL, 0, ref lopValue);
            }
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            enPausa = false;
            stopPlayBack();
        }

        private void BtnPlay_Click(object sender, EventArgs e)
        {
            if (enPausa == false)
            {
                vistaPlayback(canal, true);
            }
            else
            {
                enPausa = true;
                uint lopValue = 0;
                CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYNORMAL, 0, ref lopValue);
            }
        }

        private void BtnPause_Click(object sender, EventArgs e)
        {
            uint lopValue = 0;
            CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYPAUSE, 0, ref lopValue);
            enPausa = true;
        }

        private void BtnFast_Click(object sender, EventArgs e)
        {
            uint lopValue = 0;
            CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYFAST, 0, ref lopValue);
        }

        private void BtnSlow_Click(object sender, EventArgs e)
        {
            uint lopValue = 0;
            CHCNetSDK.NET_DVR_PlayBackControl(m_playBack, CHCNetSDK.NET_DVR_PLAYSLOW, 0, ref lopValue);
        }
        #endregion

        private string rutaCaptura;

        private void BtnCapturaPB_Click(object sender, EventArgs e)
        {
            string sJpegPicFileName;
            _capturas = @"\\192.168.3.123\cam_evt\imagenes\" + Program.idDisp + canal + @"\";
            string nombre = DateTime.Now.ToLocalTime().ToString().Replace(":", "");
            nombre = nombre.Replace("/", "").Trim();
            if (!Directory.Exists(_capturas))
            {
                Directory.CreateDirectory(_capturas);
            }
            sJpegPicFileName = _capturas + "IMG_" + nombre + ".jpeg";

            bool x = CHCNetSDK.NET_DVR_SetCapturePictureMode(1);
            if(x == false)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_SetCapturePictureMode failed, error code= " + iLastErr;
                lblEstado.Text = str;
                return;
            }

            if (!CHCNetSDK.NET_DVR_PlayBackCaptureFile(m_playBack, sJpegPicFileName))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_GetPicture_V30 failed, error code= " + iLastErr;
                lblEstado.Text = str;
                return;
            }
            else
            {
                rutaCaptura = sJpegPicFileName;
                str = "La Captura se realizó correctamente..!";
                lblEstado.Text = str;
                lblEnlace.Text = "Abrir Captura...";
            }
            conn.insertaRutaImagen(sJpegPicFileName, canal);
        }

        private void LblEnlace_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", "/select, " + rutaCaptura);
        }

        private void BtnAgregaRCP_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Está seguro de AÑADIR el cliente " + Program.codigoCli + " A la Receptora Virtual?", "Añadir Cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                conn.habilitaCuentaRCP();
                lblEstadoCliente.Text = "Conectando....!!";
                btnAgregaRCP.Enabled = false;
                Timer Cli = new Timer();
                Cli.Interval = 3000;
                Cli.Start();
                Cli.Tick += (a, b) =>
                {
                    if (conn.estadoConexion())
                    {
                        lblEstadoCliente.Text = "Conectado a la Receptora con Exito..!!";
                        Cli.Stop();
                        Cli.Dispose();
                    }
                    else
                    {
                        lblEstadoCliente.Text = "Conectando....!!";
                    }
                };
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == ">")
            {
                inicioForm();
                CHCNetSDK.NET_DVR_ZeroStopPlay(m_lRealZero);
                m_lRealZero = -1;
            }
            else
            {
                if (conn.compruebaUsuario())
                {
                    this.Location = new Point(0, 0);
                }
                this.WindowState = FormWindowState.Maximized;
                splitContainer5.Panel1Collapsed = false;
                button1.Text = ">";
                toolStrip.ImageScalingSize = new Size(24, 24);
                toolStrip1.ImageScalingSize = new Size(24, 24);

                if (DeviceInfo.byZeroChanNum > 0)
                {
                    /*for (int i = 1; i <= DeviceInfo.byZeroChanNum; i++)
                    {
                        treeView.Nodes.Add("Z" + i + " - CANAL ZERO");
                    }*/
                    canalZero();
                }
            }
        }
        
        private void inicioForm()
        {
            splitContainer5.Panel1Collapsed = true;
            this.Width = 700;
            this.Height = 735;
            int posX = 0;
            int posY = Screen.PrimaryScreen.Bounds.Height - 768;
            this.WindowState = FormWindowState.Normal;
            this.Location = new Point(posX, posY);
            button1.Text = "<";
            toolStrip.ImageScalingSize = new Size(32, 32);
            toolStrip1.ImageScalingSize = new Size(32, 32);
        }

        private void LiveView_DoubleClick(object sender, EventArgs e)
        {
            if (m_lUserID != -1)
            {
                frmFullScreen full = new frmFullScreen();
                full.Show();
            }
        }

        private void TstAudio1_Click(object sender, EventArgs e)
        {
            dwVoiceChanel = Convert.ToUInt32(tstAudio1.Tag.ToString());
            accionaAudio(0);
            
        }


    }

}