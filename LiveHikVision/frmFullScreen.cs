﻿using System;
using System.Windows.Forms;

namespace LiveHikVision
{
    public partial class frmFullScreen : Form
    {
        CHCNetSDK.NET_DVR_PREVIEWINFO prevInfo = frmLiveView.lpPreviewInfo;
        public Int32 m_lUserID = frmLiveView.m_lUserID ;
        public int canal = frmLiveView.canal;
        public int m_RealHandle = -1;
        public frmFullScreen()
        {
            InitializeComponent();

        }


        private void FrmFullScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                liveView.Dispose();
                this.Close();
                this.Dispose();
            }
        }

        private void conectar()
        {
            prevInfo.hPlayWnd = liveView.Handle;
            prevInfo.lChannel = canal;
            prevInfo.dwStreamType = 0;
            prevInfo.dwLinkMode = 0;
            prevInfo.bBlocked = false;
            prevInfo.byPreviewMode = 0;

            IntPtr pUser = new IntPtr();
            m_RealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref prevInfo, null, pUser);
        }

        private void FrmFullScreen_Load(object sender, EventArgs e)
        {
            conectar();
        }

        private void LiveView_DoubleClick(object sender, EventArgs e)
        {
            CHCNetSDK.NET_DVR_StopRealPlay(m_RealHandle);
            liveView.Dispose();
            this.Close();
            this.Dispose();
        }
    }
}
